﻿namespace MergeIssue
{
    public class MainObject
    {
        private Foo _foo;
        private Bar _bar;

        public MainObject()
        {
            _foo = new Foo();
            _bar = new Bar();
        }
    }
}
